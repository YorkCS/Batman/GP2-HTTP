# GP2 HTTP

## Prereqs

Any modern Linux or Mac running Docker 2018 CE or greater will suffice.

## Running

Running GP2 HTTP is simple using Docker Compose. This will startup [GP2I GRPC](https://gitlab.com/YorkCS/Batman/GP2I-GRPC) and [GP2D GRPC](https://gitlab.com/YorkCS/Batman/GP2D-GRPC) services for you as well as the GP2 HTTP server:

```
$ git clone git@gitlab.com:YorkCS/Batman/GP2-HTTP.git
$ cd GP2-HTTP
$ docker-compose up -d
```
