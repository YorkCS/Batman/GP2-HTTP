from grpc import insecure_channel

from gp2d_pb2 import *
from gp2d_pb2_grpc import *


class GP2DHandler(object):

    def __init__(self, client):
        self._client = client

    def handle(self, graph):
        response = self._client.Render(InputGraph(graph=graph))

        return response.graph if response.HasField('graph') else None, response.error if response.HasField('error') else None


class GP2DFactory(object):

    @staticmethod
    def make(host, port = None):
        MB = 1024 * 1024
        GRPC_CHANNEL_OPTIONS = [('grpc.max_message_length', 256 * MB), ('grpc.max_receive_message_length', 256 * MB)]

        return GP2DHandler(
            GP2DStub(insecure_channel('{}:{}'.format(host, port or 9111), options=GRPC_CHANNEL_OPTIONS))
        )
