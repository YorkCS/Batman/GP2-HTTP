from grpc import insecure_channel

from gp2i_pb2 import *
from gp2i_pb2_grpc import *


class GP2IHandler(object):

    def __init__(self, client):
        self._client = client

    def handle(self, program, graph):
        response = self._client.Interpret(Request(program=program, graph=graph))

        return response.graph if response.HasField('graph') else None, response.error if response.HasField('error') else None, response.time


class GP2IFactory(object):

    @staticmethod
    def make(host, port = None):
        MB = 1024 * 1024
        GRPC_CHANNEL_OPTIONS = [('grpc.max_message_length', 256 * MB), ('grpc.max_receive_message_length', 256 * MB)]

        return GP2IHandler(
            GP2IStub(insecure_channel('{}:{}'.format(host, port or 9111), options=GRPC_CHANNEL_OPTIONS))
        )
