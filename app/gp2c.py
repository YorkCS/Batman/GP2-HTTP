from grpc import insecure_channel

from gp2c_pb2 import *
from gp2c_pb2_grpc import *


class GP2CHandler(object):

    def __init__(self, client):
        self._client = client

    def handle(self, program):
        response = self._client.Compile(GP2Program(content=program))

        return response.code if response.HasField('code') else None, response.error if response.HasField('error') else None


class GP2CFactory(object):

    @staticmethod
    def make(host, port = None):
        MB = 1024 * 1024
        GRPC_CHANNEL_OPTIONS = [('grpc.max_message_length', 256 * MB), ('grpc.max_receive_message_length', 256 * MB)]

        return GP2CHandler(
            GP2CStub(insecure_channel('{}:{}'.format(host, port or 9111), options=GRPC_CHANNEL_OPTIONS))
        )
