import flask

from json import dumps
from os import environ

from gp2i import GP2IFactory
from gp2c import GP2CFactory
from gp2d import GP2DFactory


app = flask.Flask(__name__)

GP2I_INSTANCE = GP2IFactory.make(environ['GP2I_HOST'])
GP2C_INSTANCE = GP2CFactory.make(environ['GP2C_HOST'])
GP2D_INSTANCE = GP2DFactory.make(environ['GP2D_HOST'])


@app.route("/", methods=['GET'])
def handle_index():
    return flask.render_template('index.html')


@app.route("/source", methods=['GET'])
def handle_source():
    return flask.render_template('source.html')


@app.route("/interpret", methods=['POST'])
def handle_interpret():
    req = flask.request.get_json()

    if req is None or req.get('program') is None or req.get('graph') is None:
        return flask.Response(response=dumps({'error': 'Expected a JSON body with a program and graph.'}), status=400, mimetype="application/json")

    graph, error, time = GP2I_INSTANCE.handle(req.get('program'), req.get('graph'))

    if graph is None or graph == '':
        return flask.Response(response=dumps({'error': error, 'time': time}), status=400, mimetype="application/json")

    return flask.Response(response=dumps({'graph': graph, 'time': time}), status=200, mimetype="application/json")


@app.route("/compile", methods=['POST'])
def handle_compile():
    req = flask.request.get_json()

    if req is None or req.get('program') is None:
        return flask.Response(response=dumps({'error': 'Expected a JSON body with a program.'}), status=400, mimetype="application/json")

    code, error = GP2C_INSTANCE.handle(req.get('program'))

    if code is None or code == '':
        return flask.Response(response=dumps({'error': error}), status=400, mimetype="application/json")

    files = {}
    for file in code.files:
        if (file.name == 'Makefile' or file.name == 'build.sh' or file.name == 'gp2-compile.log'):
            continue
        files[file.name] = file.content

    return flask.Response(response=dumps({'files': dict(sorted(files.items()))}), status=200, mimetype="application/json")


@app.route("/render", methods=['POST'])
def handle_render():
    req = flask.request.get_json()

    if req is None or req.get('graph') is None:
        return flask.Response(response=dumps({'error': 'Expected a JSON body with a input graph.'}), status=400, mimetype="application/json")

    graph, error = GP2D_INSTANCE.handle(req.get('graph'))

    if graph is None or graph == '':
        return flask.Response(response=dumps({'error': error}), status=400, mimetype="application/json")

    return flask.Response(response=dumps({'graph': graph}), status=200, mimetype="application/json")


if __name__ == "__main__":
    app.run()
